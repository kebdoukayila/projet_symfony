var square = document.getElementById('square');
      var count = 0;
      var timerSpan = document.getElementById('timer');
      var timerValue = 5;
      var timerInterval = null;
      var score = 0;
      var rank = '';

      square.addEventListener('click', function() {
        count++;
      });

      timerInterval = setInterval(function() {
        timerValue--;
        timerSpan.textContent = timerValue;
        if (timerValue <= 0) {
          clearInterval(timerInterval);
          score = count;
          if (score >= 50) {
            rank = 'Excellent';
          } else if (score >= 30) {
            rank = 'Bon';
          } else if (score >= 10) {
            rank = 'Moyen';
          } else {
            rank = 'Insuffisant';
          }
          alert('Temps écoulé ! Votre score est de ' + score + '. Votre rang est ' + rank + '.');
        }
      }, 1000);

